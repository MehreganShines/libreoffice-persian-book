# راهنمای شروع کار با لیبره‌آفیس ۶


## کپی رایت

این سند توسط تیم مستندسازی لیبره‌آفیس کپی رایت می‌شود. مشارکت کنندگان در زیر فهرست شده‌اند. شما می‌توانید آن را توزیع کرده و یا آن را تحت شرایط مجوز عمومی همگانی گنو
 ( http://www.gnu.org/licenses/gpl.html)، نسخه ۳ یا بالاتر، یا مجوز اسناد مالکیت خلاقانه
(http://creativecommons.org/licenses/by/۴.۰/)، نسخه ۴.۰ یا بالاتر اصلاح کنید.
کلیه علائم تجاری در این راهنما متعلق به مالکان قانونی خود هستند.

### مشارکت‌کنندگان

 
 این کتاب از شروع با اپن‌آفیس۳.۳ اقتباس و بروزرسانی شده است.


#### به این نسخه

Andrew Jensen |Amanda Labby |Cathy Crumbley 
------------- | ----------- | ------------
Dan Lewis |Dave Barton|Jean Hollis Weber
Jorge Rodriguez|Olivier Hallot|Paul Figueiredo
Valerii Goncharuk|

#### به نسخه های قبل

Agnes Belzunce |Alan Madden|Alex Thurgood
-------:|------:|------:
Alexander Noël Dunne|Andrew Jensen|Andrew Pitonyak
Barbara M. Tobias|Carol Roberts|Chris Bonde
Claire Wood|Dan Lewis|Daniel Carrera
Dave Koelmeyer|David Michel|Gary Schnabl
Hazel Russman|Iain Roberts|Ian Laurenson
Janet M. Swisher|Jean Hollis Weber|Jared Kobos
Jeremy Cartwright|JiHui Choi|Jim Taylor
Joe Sellman|John A Smith|John Kane
Kevin O’Brien|Laurent Duperval|Leo Moons
Linda Worthington|Magnus Adielsson|Martin Fox
Martin Saffron|Michael Kotsarinis|Michel Pinquier
Michele Zarri|Miklos Vajna|Nicole Cairns
Olivier Hallot|Peter Hillier-Brook|Peter Kupfer
Peter Schofield|Rachel Kartch |Regina Henschel
Richard Barnes|Richard Detwiler| Richard Holt
Robert Scott|Ron Faile Jr.|Simon Quigley
Spencer E. Harpe|Stefan A. Keel|Steve Schwettman
Thomas Astleitner|TJ Frazier|Valerii Goncharuk



### بازخورد

لطفاً هرگونه پیشنهادها ونظرات در مورد این سند را به میلینگ لیست تیم مستندات بفرستید:
 documentation@global.libreoffice.org

> #### توجه:
> هرچیزی که به میلینگ لیست فرستاده شود، شامل ایمیل شما و هر اطلاعات شخصی دیگری که در پیام نوشته شده باشه، به صورت عمومی بایگانی شده و غیرقابل حذف است.

### تاریخ انتشار و نسخه برنامه

اگوست ۲۰۱۸. بر اساس لیبره‌آفیس ۶٫۰.
